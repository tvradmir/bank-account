package com.account.domain;

import java.math.BigDecimal;

import com.account.exception.InsufficientBalanceException;
import com.account.exception.InvalidAmountException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class AccountTest {


	@Test
	void should_return_deposit_operation_with_amount_50_when_deposit_50() {
		// given
		Account account = new Account("5012K65", BigDecimal.valueOf(100));

		// when
		Operation actualOperation = account.deposit(BigDecimal.valueOf(50));
		// then
		assertEquals(OperationType.DEPOSIT, actualOperation.getType());
		assertEquals(BigDecimal.valueOf(50), actualOperation.getAmount());
		assertNotNull(actualOperation.getDate());

	}



	@Test
	void should_return_deposit_operation_with_amount_100_when_deposit_100() {
		// given
		Account account = new Account("5012K65", BigDecimal.valueOf(100));

		// when
		Operation actualOperation = account.deposit(BigDecimal.valueOf(100));
		// then
		assertEquals(OperationType.DEPOSIT, actualOperation.getType());
		assertEquals(BigDecimal.valueOf(100), actualOperation.getAmount());

	}

	@Test
	void should_return_deposit_operation_with_previous_balance_50_deposit_50_given_account_with_balance_50() {
		// given
		Account account = new Account("5012K65", BigDecimal.valueOf(50));

		// when
		Operation actualOperation = account.deposit(BigDecimal.valueOf(50));

		// then
		assertEquals(OperationType.DEPOSIT, actualOperation.getType());
		assertEquals(BigDecimal.valueOf(50), actualOperation.getPreviousBalance());
		assertEquals(account, actualOperation.getAccount());

	}

	@Test
	void should_return_deposit_operation_with_previous_balance_100_deposit_50_given_account_with_balance_100() {
		// given
		Account account = new Account( "5012K65", BigDecimal.valueOf(100));

		// when
		Operation actualOperation = account.deposit(BigDecimal.valueOf(50));

		// then
		assertEquals(OperationType.DEPOSIT, actualOperation.getType());
		assertEquals(BigDecimal.valueOf(100), actualOperation.getPreviousBalance());

	}


	@Test
	void should_return_deposit_operation_with_new_balance_150_deposit_50_given_account_with_balance_100() {
		// given
		Account account = new Account("5012K65", BigDecimal.valueOf(100));

		// when
		Operation actualOperation = account.deposit(BigDecimal.valueOf(50));

		// then
		assertEquals(OperationType.DEPOSIT, actualOperation.getType());
		assertEquals(BigDecimal.valueOf(150), actualOperation.getNewBalance());

	}

	@Test
	void should_return_deposit_operation_with_new_balance_200_deposit_100_given_account_with_balance_100() {
		// given
		Account account = new Account("5012K65", BigDecimal.valueOf(100));

		// when
		Operation actualOperation = account.deposit(BigDecimal.valueOf(100));

		// then
		assertEquals(OperationType.DEPOSIT, actualOperation.getType());
		assertEquals(BigDecimal.valueOf(200), actualOperation.getNewBalance());

	}

	@Test
	void should_throwInvalidAmountException_when_deposit_0_amount() {
		Account account = new Account("5012K65", BigDecimal.valueOf(100));
		assertThrows(InvalidAmountException.class, () -> account.deposit(BigDecimal.ZERO));

	}


	@Test
	void should_throwInvalidAmountException_when_deposit_minus_50_amount() {
		Account account = new Account("5012K65", BigDecimal.valueOf(100));
		assertThrows(InvalidAmountException.class, () -> account.deposit(BigDecimal.valueOf(-50)));

	}

	@Test
	void should_throwInvalidAmountException_when_deposit_minus_100_amount() {
		Account account = new Account("5012K65", BigDecimal.valueOf(100));
		assertThrows(InvalidAmountException.class, () -> account.deposit(BigDecimal.valueOf(-100)));

	}



	@Test
	void should_return_withdrawal_operation_with_amount_50_when_withdraw_50() {
		// given
		Account account = new Account( "5012K65", BigDecimal.valueOf(100));

		// when
		Operation actualOperation = account.withdraw(BigDecimal.valueOf(50));
		// then
		assertEquals(OperationType.WITHDRAWAL, actualOperation.getType());
		assertEquals(BigDecimal.valueOf(50), actualOperation.getAmount());

	}




	@Test
	void should_return_withdrawal_operation_with_amount_100_when_withdraw_100() {
		// given
		Account account = new Account( "5012K65", BigDecimal.valueOf(100));

		// when
		Operation actualOperation = account.withdraw(BigDecimal.valueOf(100));
		// then
		assertEquals(OperationType.WITHDRAWAL, actualOperation.getType());
		assertEquals(BigDecimal.valueOf(100), actualOperation.getAmount());

	}

	@Test
	void should_return_withdrawal_operation_with_previous_balance_50_withdraw_50_given_account_with_balance_50() {
		// given
		Account account = new Account("5012K65", BigDecimal.valueOf(50));

		// when
		Operation actualOperation = account.withdraw(BigDecimal.valueOf(50));

		// then
		assertEquals(OperationType.WITHDRAWAL, actualOperation.getType());
		assertEquals(BigDecimal.valueOf(50), actualOperation.getPreviousBalance());

	}

	@Test
	void should_return_withdrawal_operation_with_previous_balance_100_withdraw_50_given_account_with_balance_100() {
		// given
		Account account = new Account("5012K65", BigDecimal.valueOf(100));

		// when
		Operation actualOperation = account.withdraw(BigDecimal.valueOf(50));

		// then
		assertEquals(OperationType.WITHDRAWAL, actualOperation.getType());
		assertEquals(BigDecimal.valueOf(100), actualOperation.getPreviousBalance());

	}


	@Test
	void should_return_withdrawal_operation_with_new_balance_50_withdraw_50_given_account_with_balance_100() {
		// given
		Account account = new Account("5012K65", BigDecimal.valueOf(100));

		// when
		Operation actualOperation = account.withdraw(BigDecimal.valueOf(50));

		// then
		assertEquals(OperationType.WITHDRAWAL, actualOperation.getType());
		assertEquals(BigDecimal.valueOf(50), actualOperation.getNewBalance());

	}

	@Test
	void should_return_withdrawal_operation_with_new_balance_150_point_35_withdraw_25_given_account_with_balance_175_point_35() {
		// given
		Account account = new Account("5012K65", BigDecimal.valueOf(175.35));

		// when
		Operation actualOperation = account.withdraw(BigDecimal.valueOf(25));

		// then
		assertEquals(OperationType.WITHDRAWAL, actualOperation.getType());
		assertEquals(BigDecimal.valueOf(150.35), actualOperation.getNewBalance());

	}

	@Test
	void should_return_withdrawal_operation_with_new_balance_0_withdraw_100_given_account_with_balance_100() {
		// given
		Account account = new Account( "5012K65", BigDecimal.valueOf(100));

		// when
		Operation actualOperation = account.withdraw(BigDecimal.valueOf(100));

		// then
		assertEquals(OperationType.WITHDRAWAL, actualOperation.getType());
		assertEquals(BigDecimal.valueOf(0), actualOperation.getNewBalance());

	}

	@Test
	void should_throwInvalidAmountException_when_withdrawal_0_amount() {
		Account account = new Account("5012K65", BigDecimal.valueOf(100));
		assertThrows(InvalidAmountException.class, () -> account.withdraw(BigDecimal.ZERO));

	}


	@Test
	void should_throwInvalidAmountException_when_withdrawal_minus_50_amount() {
		Account account = new Account("5012K65", BigDecimal.valueOf(100));
		assertThrows(InvalidAmountException.class, () -> account.withdraw(BigDecimal.valueOf(-50)));

	}

	@Test
	void should_throwInvalidAmountException_when_withdrawal_minus_100_amount() {
		Account account = new Account("5012K65", BigDecimal.valueOf(100));
		assertThrows(InvalidAmountException.class, () -> account.withdraw(new BigDecimal(-100)));

	}


	@Test
	void should_throwInsufficientBalanceException_when_withdrawal_100_amount_given_account_with_balance_50() {
		Account account = new Account("5012K65", BigDecimal.valueOf(50));
		assertThrows(InsufficientBalanceException.class, () -> account.withdraw(new BigDecimal(100)));

	}


	@Test
	void should_throwInsufficientBalanceException_when_withdrawal_95_amount_given_account_with_balance_90() {
		Account account = new Account("5012K65", BigDecimal.valueOf(90));
		assertThrows(InsufficientBalanceException.class, () -> account.withdraw(new BigDecimal(95)));

	}











}
