package com.account.services;

import com.account.domain.Operation;
import com.account.domain.OperationType;
import com.account.port.dsi.OperationAccountPort;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import static org.mockito.BDDMockito.*;;

public class BankAccountOperationHistoryServiceTest {

    private final OperationAccountPort operationAccountPort =  mock(OperationAccountPort.class);

    private BankAccountOperationHistoryService bankAccountOperationHistoryService = new BankAccountOperationHistoryService(operationAccountPort);


    @Test
    public void should_return_empty_list_when_load_account_operation() {
        given(operationAccountPort.loadAccountOperations(anyString(), any(LocalDate.class))).willReturn(new ArrayList<>());
        String accountNumber = "BK1234";
        LocalDate fromDate = LocalDate.now();
        List<Operation> operations = bankAccountOperationHistoryService.getBankAccountHistory(accountNumber, fromDate);

        then(operationAccountPort).should().loadAccountOperations(accountNumber, fromDate);
        then(operations.size()).equals(0);
    }


    @Test
    public void should_return_list_of_one_operation_when_load_account_operations_given_one_operation() {

        Operation expectedOperation = mock(Operation.class);

        given(operationAccountPort.loadAccountOperations(anyString(), any(LocalDate.class))).willReturn(Arrays.asList(expectedOperation));
        String accountNumber = "BK1234";
        LocalDate fromDate = LocalDate.now().minusDays(3);
        List<Operation> operations = bankAccountOperationHistoryService.getBankAccountHistory(accountNumber, fromDate);

        then(operationAccountPort).should().loadAccountOperations(accountNumber, fromDate);
        then(operations.size()).equals(1);
        then(operations.get(0)).equals(expectedOperation);

    }

    @Test
    public void should_return_list_of_two_operations__operation_when_load_account_operations_given_two_operation() {


        List<Operation> expectedOperations = Arrays.asList(mock(Operation.class), mock(Operation.class));
        given(operationAccountPort.loadAccountOperations(anyString(), any(LocalDate.class))).willReturn(expectedOperations);
        String accountNumber = "BK1234";
        LocalDate fromDate = LocalDate.now().minusDays(3);
        List<Operation> operations = bankAccountOperationHistoryService.getBankAccountHistory(accountNumber, fromDate);

        then(operationAccountPort).should().loadAccountOperations(accountNumber, fromDate);
        then(operations.size()).equals(2);
        then(operations).equals(expectedOperations);

    }
}
