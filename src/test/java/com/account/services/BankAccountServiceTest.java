package com.account.services;

import com.account.domain.Account;
import com.account.domain.Operation;
import com.account.exception.InsufficientBalanceException;
import com.account.exception.InvalidAmountException;
import com.account.port.dsi.AccountPort;
import com.account.port.dsi.OperationAccountPort;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.*;

public class BankAccountServiceTest {

    private final AccountPort accountPort = mock(AccountPort.class);
    private final OperationAccountPort operationAccountPort =  mock(OperationAccountPort.class);
    private BankAccountService bankAccountService = new BankAccountService(accountPort,operationAccountPort);

    private Account account = mock(Account.class);
    private Operation operation = mock(Operation.class);

    private String accountNumber = "BP0697";
    private BigDecimal amount = BigDecimal.valueOf(100);


    @Test
    public void should_success_when_deposit_amount_100_on_bank_account_BP0697() {

        given(account.deposit(any(BigDecimal.class))).willReturn(operation);
        given(accountPort.loadAccount(anyString())).willReturn(Optional.of(account));

        boolean success = bankAccountService.deposit(accountNumber, amount);
        assertTrue(success);

        then(accountPort).should().loadAccount(accountNumber);
        then(account).should().deposit(amount);
        then(operation).should().getNewBalance();
        then(operationAccountPort).should().saveAccountOperation(operation);
        then(accountPort).should().updateAccountBalance(account, operation.getNewBalance());

    }



    @Test
    public void should_fail_when_deposit_amount_100_on_non_existing_bank_account() {
        given(accountPort.loadAccount(any())).willReturn(Optional.empty());
        boolean success = bankAccountService.deposit(accountNumber, amount);
        assertFalse(success);

        then(accountPort).should().loadAccount(accountNumber);

    }


    @Test
    public void should_throws_invalid_amount_exception_when_deposit_invalid_amount() {

        given(account.deposit(any(BigDecimal.class))).willThrow(InvalidAmountException.class);
        given(accountPort.loadAccount(anyString())).willReturn(Optional.of(account));

        assertThrows(InvalidAmountException.class, ()-> bankAccountService.deposit(accountNumber, amount));
        then(accountPort).should().loadAccount(accountNumber);


    }


    @Test
    public void should_success_when_withdraw_amount_100_on_bank_account_BP0697() {

        given(account.withdraw(any(BigDecimal.class))).willReturn(operation);
        given(accountPort.loadAccount(anyString())).willReturn(Optional.of(account));

        boolean success = bankAccountService.withDraw(accountNumber, amount);
        assertTrue(success);

        then(accountPort).should().loadAccount(accountNumber);
        then(account).should().withdraw(amount);
        then(operation).should().getNewBalance();
        then(accountPort).should().updateAccountBalance(account, operation.getNewBalance());

    }



    @Test
    public void should_fail_when_withdraw_amount_100_on_non_existing_bank_account() {
        given(accountPort.loadAccount(any())).willReturn(Optional.empty());
        boolean success = bankAccountService.withDraw(accountNumber, amount);
        assertFalse(success);

        then(accountPort).should().loadAccount(accountNumber);

    }


    @Test
    public void should_throws_invalid_amount_exception_when_withdraw_invalid_amount() {

        given(account.withdraw(any(BigDecimal.class))).willThrow(InvalidAmountException.class);
        given(accountPort.loadAccount(anyString())).willReturn(Optional.of(account));

        assertThrows(InvalidAmountException.class, ()-> bankAccountService.withDraw(accountNumber, amount));
        then(accountPort).should().loadAccount(accountNumber);


    }


    @Test
    public void should_throws_insufficient_balance_exception_when_withdraw_in_invalid_amount() {

        given(account.withdraw(any(BigDecimal.class))).willThrow(InsufficientBalanceException.class);
        given(accountPort.loadAccount(anyString())).willReturn(Optional.of(account));

        assertThrows(InsufficientBalanceException.class, ()-> bankAccountService.withDraw(accountNumber, amount));
        then(accountPort).should().loadAccount(accountNumber);


    }



}
