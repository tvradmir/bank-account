package com.account.services;

import com.account.domain.Account;
import com.account.domain.Operation;
import com.account.port.api.DepositUseCase;
import com.account.port.api.WithdrawUseCase;
import com.account.port.dsi.AccountPort;
import com.account.port.dsi.OperationAccountPort;
import lombok.AllArgsConstructor;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

@AllArgsConstructor
public class BankAccountService implements DepositUseCase, WithdrawUseCase {

    private final AccountPort accountPort;
    private final OperationAccountPort operationAccountPort;

    @Override
    public boolean deposit(String accountNumber, BigDecimal amount) {
        AtomicBoolean success = new AtomicBoolean(false);
        Optional<Account> optionalAccount = accountPort.loadAccount(accountNumber);
        optionalAccount.ifPresent(
                account -> {
                    Operation operation = account.deposit(amount);
                    operationAccountPort.saveAccountOperation(operation);
                    accountPort.updateAccountBalance(account,operation.getNewBalance());
                    success.set(true);
                    }
        );
        return success.get();
    }

    @Override
    public boolean withDraw(String accountNumber, BigDecimal amount) {
        AtomicBoolean success = new AtomicBoolean(false);
        Optional<Account> optionalAccount = accountPort.loadAccount(accountNumber);
        optionalAccount.ifPresent(
                account -> {
                    Operation operation = account.withdraw(amount);
                    operationAccountPort.saveAccountOperation(operation);
                    accountPort.updateAccountBalance(account,operation.getNewBalance());
                    success.set(true);
                }
        );
        return success.get();
    }
}
