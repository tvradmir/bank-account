package com.account.services;

import com.account.domain.Operation;
import com.account.port.api.BankAccountOparationHistoryUseCase;
import com.account.port.dsi.OperationAccountPort;
import lombok.AllArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
public class BankAccountOperationHistoryService implements BankAccountOparationHistoryUseCase{

    private  final OperationAccountPort operationAccountPort;

    @Override
    public List<Operation> getBankAccountHistory(String accountNumber, LocalDate fromDate) {
        return operationAccountPort.loadAccountOperations(accountNumber, fromDate);
    }
}
