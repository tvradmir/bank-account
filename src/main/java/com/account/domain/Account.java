package com.account.domain;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.account.exception.InsufficientBalanceException;
import com.account.exception.InvalidAmountException;
import lombok.NonNull;
import lombok.Value;

@Value
public class Account {

	@NonNull
	private final String accountNumber;
	@NonNull
	private final BigDecimal accountBalance;


	public Operation deposit(@NonNull BigDecimal amount) {
		if (isAmountLessThanOrEqualToZero(amount)) {
			throw new InvalidAmountException("amount should not be less than or equal to zero");
		}
		return new Operation(this, OperationType.DEPOSIT, LocalDateTime.now(),amount, accountBalance, accountBalance.add(amount));
	}

	private boolean isAmountLessThanOrEqualToZero(@NonNull BigDecimal amount) {
		return amount.compareTo(BigDecimal.ZERO) <= 0;
	}


	public Operation withdraw(BigDecimal amount) {
		if (isAmountLessThanOrEqualToZero(amount)) {
			throw new InvalidAmountException("amount should not be less than or equal to zero");
		}
		if(hasInsufficientBalance(amount)){
			throw new InsufficientBalanceException("insufficient balance");
		}
		return (new Operation(this,OperationType.WITHDRAWAL, LocalDateTime.now(),amount,accountBalance, accountBalance.subtract(amount)));
	}

	private boolean hasInsufficientBalance(BigDecimal amount) {
		return accountBalance.subtract(amount).compareTo(BigDecimal.ZERO) < 0;
	}
}
