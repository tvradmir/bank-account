package com.account.domain;

import lombok.NonNull;
import lombok.Value;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Value
public class Operation {
    @NonNull
    private Account account;
    @NonNull
    private OperationType type;
    @NonNull
    private LocalDateTime date;
    @NonNull
    private BigDecimal amount;
    @NonNull
    private BigDecimal previousBalance;
    @NonNull
    private BigDecimal newBalance;

}
