package com.account.domain;

public enum OperationType {
    DEPOSIT, WITHDRAWAL;
}
