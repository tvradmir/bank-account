package com.account.exception;

public class InvalidAmountException extends ServiceException {

	public InvalidAmountException(String error) {
		super(error);
	}

}
