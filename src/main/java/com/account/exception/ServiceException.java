package com.account.exception;

public abstract class ServiceException extends IllegalStateException {
	public ServiceException(String error) {
		super(error);
	}
}
