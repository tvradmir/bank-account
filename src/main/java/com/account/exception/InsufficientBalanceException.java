package com.account.exception;


public class InsufficientBalanceException extends ServiceException {

    public InsufficientBalanceException(String error) {
        super(error);
    }
}
