package com.account.port.dsi;

import com.account.domain.Account;

import java.math.BigDecimal;
import java.util.Optional;

public interface AccountPort {

    Optional<Account> loadAccount(String accountNumber);
    Account updateAccountBalance(Account  account, BigDecimal newBalance);
}
