package com.account.port.dsi;

import com.account.domain.Operation;

import java.time.LocalDate;
import java.util.List;

public interface OperationAccountPort {
    List<Operation> loadAccountOperations(String accountNumber, LocalDate fromDate);
    Operation saveAccountOperation(Operation operation);
}
