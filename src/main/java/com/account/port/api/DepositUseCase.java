package com.account.port.api;

import java.math.BigDecimal;

public interface DepositUseCase {

    boolean deposit(String accountNumber , BigDecimal amount);

}
