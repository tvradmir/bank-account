package com.account.port.api;

import com.account.domain.Operation;

import java.time.LocalDate;
import java.util.List;

public interface BankAccountOparationHistoryUseCase {

    List<Operation> getBankAccountHistory(String accountNumber, LocalDate fromDate);

}
