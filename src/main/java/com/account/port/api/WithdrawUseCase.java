package com.account.port.api;

import java.math.BigDecimal;

public interface WithdrawUseCase {

    boolean withDraw(String accountNumber , BigDecimal amount);
}
